package backend.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.UserRepository;
import timetolive.backend.service.AuthenticationService;

import java.util.Collections;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserRepository.class, AuthenticationService.class})

public class AuthIntegrationTest {

    @MockBean
    UserRepository userRepository;

    @Autowired
    AuthenticationService authenticationService;

    @Test
    //@Disabled
    public void authIntegration_invalidCredentials_deny() {
        UserEntity t = new UserEntity("safaef","wdswsd","MTIz");

        Mockito.when(userRepository.save(t)).thenReturn(t);
        Mockito.when(userRepository.findByEmail("safaef")).thenReturn(Optional.of(t));


        Assertions.assertDoesNotThrow(()-> {
            UserEntity userEntity = authenticationService.loginUser("safaef", "1234");
            Assertions.assertFalse(t == userEntity);
        });
    }

    @Test
    //@Disabled
    public void authIntegration_validCredentials_deny() {
        UserEntity t = new UserEntity("safaef","wdswsd","MTIz");

        Mockito.when(userRepository.save(t)).thenReturn(t);
        Mockito.when(userRepository.findByEmail("safaef")).thenReturn(Optional.of(t));


        Assertions.assertDoesNotThrow(()-> {
            UserEntity userEntity = authenticationService.loginUser("safaef", "123");
            Assertions.assertTrue(t == userEntity);
        });
    }
    @Test
    //@Disabled
    public void authIntegration_validLogout() {
        UserEntity t = new UserEntity("token");

        Mockito.when(userRepository.save(t)).thenReturn(t);
        Mockito.when(userRepository.findByToken("token")).thenReturn(Collections.singletonList(t));


        Assertions.assertDoesNotThrow(()-> {
            if(authenticationService.logout("token")) {
                Assertions.assertTrue(t.getToken().equals(""));
            }
        });
    }
}
