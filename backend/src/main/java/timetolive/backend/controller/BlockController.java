package timetolive.backend.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import timetolive.backend.controller.jsonModel.requests.BlockDeleteRequest;
import timetolive.backend.controller.jsonModel.requests.ClockRequest;
import timetolive.backend.controller.jsonModel.requests.FolderRequest;
import timetolive.backend.entity.Clocks;
import timetolive.backend.service.BlockService;


import static timetolive.backend.configuration.SpringFoxConfig.BLOCK_TAG;

@Slf4j
@RestController
@Api(tags = {BLOCK_TAG})
public class BlockController {
    @Autowired
    private final BlockService service = null;

    @PostMapping("/createBlock")
    public GeneralResponse<Long> createClock(@RequestBody ClockRequest clockRequest) {
        log.info("Create clock block");

        Clocks clock = service.createClock(clockRequest.getFolderName(), clockRequest.getToken(), clockRequest.getX(),
                clockRequest.getY(), clockRequest.getHeight(), clockRequest.getWidth());
        if(clock == null)
        {
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK, clock.getBlock_parameter_id());
    }

    @DeleteMapping("/deleteBlock")
    public GeneralResponse deleteFolders(@RequestBody BlockDeleteRequest blockDeleteRequest) {
        log.info("Delete clock");

        if(!service.delete(blockDeleteRequest.getBlockId(), blockDeleteRequest.getToken())){
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK);
    }
}

