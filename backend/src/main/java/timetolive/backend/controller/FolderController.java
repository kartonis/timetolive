package timetolive.backend.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import timetolive.backend.controller.jsonModel.requests.FolderRequest;
import timetolive.backend.controller.jsonModel.responses.BlockInfo;
import timetolive.backend.controller.jsonModel.responses.FolderInfoResponse;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.service.FoldersService;

import java.util.LinkedList;
import java.util.List;

import static timetolive.backend.configuration.SpringFoxConfig.FOLDERS_TAG;

@Slf4j
@RestController
@Api(tags = {FOLDERS_TAG})
public class FolderController {
    @Autowired
    private final FoldersService service = null;

    @PostMapping("/createFolder")
    public GeneralResponse createFolders(@RequestBody FolderRequest createFolderRequest) {
        log.info("Create new folder");

        if(!service.create(createFolderRequest.getName(), createFolderRequest.getToken())){
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK);
    }

    @GetMapping("/folderInfo")
    public GeneralResponse<FolderInfoResponse> getFolderInfo(@RequestBody FolderRequest folderRequest) {
        log.info("Get blocks with folder");

        List<BlockParameter> blocks = service.getBlocks(folderRequest.getToken(), folderRequest.getName());
        if(blocks == null)
        {
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        List<BlockInfo> blockInfos = new LinkedList<>();

        for(BlockParameter blockParameter : blocks)
        {
            blockInfos.add(new BlockInfo(blockParameter.getBlock_parameter_id(), blockParameter.getHeight(),
                    blockParameter.getWidth(), blockParameter.getX(), blockParameter.getY()));
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK, new FolderInfoResponse(blockInfos, folderRequest.getToken()));
    }

    @DeleteMapping("/deleteFolder")
    public GeneralResponse deleteFolders(@RequestBody FolderRequest deleteFolderRequest) {
        log.info("Delete folder");

        if(!service.delete(deleteFolderRequest.getName(), deleteFolderRequest.getToken())){
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK);
    }
}
