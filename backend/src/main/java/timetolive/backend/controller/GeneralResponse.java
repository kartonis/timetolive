package timetolive.backend.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

@Setter
@Getter
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GeneralResponse<T> {
    public enum Status
    {
        OK,
        BAD_REQUEST
    }
    private final Status status;
    private final T data;

    public GeneralResponse(Status status){
        this.status = status;
        data = null;
    }
}