package timetolive.backend.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import timetolive.backend.controller.jsonModel.requests.LoginRequest;
import timetolive.backend.controller.jsonModel.requests.LogoutRequest;
import timetolive.backend.controller.jsonModel.responses.LoginResponse;
import timetolive.backend.controller.jsonModel.requests.RegistrationRequest;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.service.AuthenticationService;

import java.util.List;
import java.util.stream.Collectors;

import static timetolive.backend.configuration.SpringFoxConfig.AUTHENTICATION_TAG;

@Slf4j
@RestController
@Api(tags = {AUTHENTICATION_TAG})
public class AuthenticationController {
    @Autowired
    private final AuthenticationService service = null;

    @PostMapping("/register")
    public GeneralResponse registration(@RequestBody RegistrationRequest registrationRequest) {
        log.info("Load user's data");

        if(!service.registration(new UserEntity(registrationRequest.getEmail(), registrationRequest.getName(),
                registrationRequest.getPassword()))) {
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        return new GeneralResponse<>(GeneralResponse.Status.OK);
    }

    @PostMapping("/login")
    public GeneralResponse<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
        log.info("Get user's data");
        UserEntity user = service.loginUser(loginRequest.getEmail(), loginRequest.getPassword());
        if(user == null)
        {
            return new GeneralResponse<>(GeneralResponse.Status.BAD_REQUEST);
        }
        List<String> folders = user.getFolders().stream().map(Folder::getName).collect(Collectors.toList());
        return new GeneralResponse<>(GeneralResponse.Status.OK,
                new LoginResponse(folders, user.getToken()));
    }

    @PostMapping("/logout")
    public GeneralResponse logout(@RequestBody LogoutRequest logoutRequest) {
        log.info("Delete user's token");
        service.logout(logoutRequest.getToken());
        return new GeneralResponse<>(GeneralResponse.Status.OK);
    }

}
