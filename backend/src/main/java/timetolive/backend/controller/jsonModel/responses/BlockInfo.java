package timetolive.backend.controller.jsonModel.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlockInfo {
    private long blockId;
    private double height;
    private double width;
    private double x;
    private double y;
}
