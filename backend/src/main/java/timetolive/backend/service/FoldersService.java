package timetolive.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.BlockParameterRepository;
import timetolive.backend.repository.FolderRepository;
import timetolive.backend.repository.UserRepository;

import java.util.LinkedList;
import java.util.List;

@Service
public class FoldersService {

    @Autowired
    private final UserRepository userRepository = null;
    @Autowired
    private final FolderRepository folderRepository = null;
    @Autowired
    private final BlockParameterRepository blockParameterRepository = null;

    public boolean create(String name, String token) {
        Iterable<UserEntity> iterable = userRepository.findByToken(token);
        if (!iterable.iterator().hasNext()) {
            return false;
        }
        UserEntity user = iterable.iterator().next();

        folderRepository.save(new Folder(name, user));
        return true;
    }

    public List<BlockParameter> getBlocks(String token, String folderName){
        List<BlockParameter> blocks = new LinkedList<>();

        Iterable<UserEntity> iterable = userRepository.findByToken(token);
        if (!iterable.iterator().hasNext()) {
            return null;
        }
        UserEntity user = iterable.iterator().next();
        Iterable<Folder> folderIterable = folderRepository.findByUser(user);

        folderIterable.forEach(folder -> {
            if (folder.getName().equals(folderName)) {
                blocks.add(blockParameterRepository.findByFolder(folder));
            }
        });
        return blocks;
    }

    public boolean delete(String name, String token) {
        Iterable<UserEntity> iterable = userRepository.findByToken(token);
        if (!iterable.iterator().hasNext()) {
            return false;
        }
        UserEntity user = iterable.iterator().next();
        Iterable<Folder> folderIterable = folderRepository.findByUser(user);

        folderIterable.forEach(folder -> {
            if (folder.getName().equals(name)) {
                folderRepository.delete(folder);
            }
            });
        return true;
    }
}

