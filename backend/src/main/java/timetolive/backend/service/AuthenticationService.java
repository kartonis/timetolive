package timetolive.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.UserRepository;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthenticationService {

    @Autowired
    private final UserRepository userRepository = null;

    public UserEntity loginUser(String email, String password) {
        Optional<UserEntity> opt =  userRepository.findByEmail(email);
        if (!opt.isPresent()) {
            return null;
        }
        UserEntity user = opt.get();

        if (!decryptPassword(user.getPassword()).equals(password))
        {
            return null;
        }

        String token = user.getId() + ":" + UUID.randomUUID();
        user.setToken(token);
        System.out.println(token);
        user = userRepository.save(user);
        return user;
    }

    private String encryptPassword(String password) {

        return Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
    }

    private String decryptPassword(String password) {
        byte[] decodedBytes = Base64.getDecoder().decode(password);
        return new String(decodedBytes);
    }


    public boolean registration(UserEntity user) {
            user.setPassword(encryptPassword(user.getPassword()));

        try {
            userRepository.save(user);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public boolean logout(String token)
    {
        Iterator<UserEntity> users = userRepository.findByToken(token).iterator();
        if(users.hasNext()) {
            UserEntity user = users.next();
            user.setToken("");
            userRepository.save(user);
            return true;
        }
        return false;
    }


    public String getId(String token){
        return token.split(":")[0];
    }
}
