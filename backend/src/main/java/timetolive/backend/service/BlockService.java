package timetolive.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Clocks;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.BlockParameterRepository;
import timetolive.backend.repository.ClockRepository;
import timetolive.backend.repository.FolderRepository;
import timetolive.backend.repository.UserRepository;

import java.util.Optional;

@Service
public class BlockService {

    @Autowired
    private final UserRepository userRepository = null;
    @Autowired
    private final FolderRepository folderRepository = null;
    @Autowired
    private final ClockRepository clockRepository = null;

    public Clocks createClock(String folderName, String token, double x, double y, double height, double width) {
        Iterable<UserEntity> iterable = userRepository.findByToken(token);
        if (!iterable.iterator().hasNext()) {
            return null;
        }
        UserEntity user = iterable.iterator().next();

        Iterable<Folder> iterableFolder = folderRepository.findByUser(user);

        while(iterableFolder.iterator().hasNext())
        {
            Folder folder = iterableFolder.iterator().next();
            if(folder.getName().equals(folderName))
            {
                Clocks clocks = new Clocks(x, y, height, width, folder);
                clockRepository.save(clocks);
                return  clocks;
            }
        }
        return null;
    }

    public boolean delete(Long blockId, String token) {
        Iterable<UserEntity> iterable = userRepository.findByToken(token);
        if (!iterable.iterator().hasNext()) {
            return false;
        }
        UserEntity user = iterable.iterator().next();
        Iterable<Folder> folderIterable = folderRepository.findByUser(user);

        Optional<Clocks> clocks = clockRepository.findById(blockId);
        folderIterable.forEach(folder -> {
            if (clocks.get().getBlock_parameter_id() == blockId) {
                clockRepository.delete(clocks.get());
            }
        });
        return true;
    }
}


