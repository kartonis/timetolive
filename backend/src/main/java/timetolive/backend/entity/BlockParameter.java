package timetolive.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "block_parameters")
@Inheritance(strategy = InheritanceType.JOINED)

public class BlockParameter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long block_parameter_id;
    private double x;
    private double y;
    private double height;
    private double width;

    @ManyToOne()
    @JoinColumn(name = "folder_id", nullable=false)
    private Folder folder;

    public BlockParameter(Folder folder, double x, double y, double height, double width) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.folder = folder;
    }

}
