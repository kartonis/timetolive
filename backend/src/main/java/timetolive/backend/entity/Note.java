package timetolive.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notes")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long note_id;

//    @ManyToOne()
//    @JoinColumn(name = "block_parameter_id", nullable=false)
//    private BlockParameter blockParameter;

    @ManyToOne()
    @JoinColumn(name = "folder_id", nullable=false)
    private Folder folder;
}