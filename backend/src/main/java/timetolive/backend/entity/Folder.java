package timetolive.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "folders")
public class Folder {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE,
            generator="folders_seq")
    @SequenceGenerator(name="folders_seq",
            sequenceName="folders_folder_id_seq", allocationSize=10)
    @Column(name="folder_id", updatable=false, nullable=false)
    private int folder_id;

    @Column(unique = true)
    private String name;

    @ManyToOne() //во вкладках ссылка на юзера
    @JoinColumn(name = "user_id", nullable=false)
    private UserEntity user;

    public Folder(String name){
        this.name = name;
    }

    public Folder(String name, UserEntity userEntity){
        this.name = name;
        this.user = userEntity;
    }
}
