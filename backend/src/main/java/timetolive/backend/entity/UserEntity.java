package timetolive.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import timetolive.backend.entity.Folder;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String email;
    private String name;
    private String password;
    private String token;

    public UserEntity(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.folders = new LinkedList<>();
    }

    public UserEntity(String token) {
        this.token = token;
    }

    public UserEntity(String email, String name, String password, String token, List<Folder> folders) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.token = token;
        this.folders = folders;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user") //в юзере ничего нет
    List<Folder> folders;

}
