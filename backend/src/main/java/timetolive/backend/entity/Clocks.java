package timetolive.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "clocks")
public class Clocks extends BlockParameter{
    public Clocks(double x, double y, double height, double width, Folder folder)
    {
        super(folder, x, y, height, width);
    }
}
